import * as recast from 'recast'
import * as fs from 'fs'
import * as path from 'path'
import {glob} from 'glob'

const handleFile = (filePath: string) => {
  const fileFolder = path.dirname(filePath)
  const value = fs.readFileSync(filePath, 'utf8')
  const parsed = recast.parse(value, {
    parser: require('recast/parsers/babel')
  })
  recast.types.visit(parsed, {
    visitImportDeclaration: importDeclaration => {
      const value = importDeclaration.node.source.value
      if (typeof value === 'string' && (value.startsWith('.') || value.startsWith('..')) && !value.endsWith('.js')) {
        const fileName = path.join(fileFolder, value + '.js')
        const folderName = path.join(fileFolder, value, 'index.js')
        if (fs.existsSync(fileName)) {
          importDeclaration.node.source.value += '.js'
        } else if (fs.existsSync(folderName)) {
          importDeclaration.node.source.value += '/index.js'
        }
      }
      return false
    }
  })
  fs.writeFileSync(filePath, recast.print(parsed).code)
}

const sourcePath = process.argv[2]
const files = glob.sync(`${sourcePath}/**/*.{js,jsx,ts,tsx}`, {ignore: `${sourcePath}/**/node_modules/**`})

console.log(`Checking ${files.length} files`)
files.forEach((file, index) => {
  handleFile(file)
  console.log(`File ${index} finished`)
})
